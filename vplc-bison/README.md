# Image Docker pour les APNEEs d'Analyse Syntaxique

1. Se logger au dépôt du container : 
  ```docker login gricad-registry.univ-grenoble-alpes.fr```

2. Construire l'image en la taggant avec le bon dépôt
```docker build -t gricad-registry.univ-grenoble-alpes.fr/serassgi/caseine-docker-stacks/vplc-bison2 .```
3. Pousser l'image pour la rendre disponible
```docker push  gricad-registry.univ-grenoble-alpes.fr/serassgi/caseine-docker-stacks/vplc-bison2```
